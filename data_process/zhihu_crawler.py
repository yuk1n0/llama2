import requests
import time
import os
import re
import json


def get_zhihu_results(qid_list, 
                      output_dataset_dir="./crawler_result/zhihu_response.tsv", 
                      retry=10, 
                      bad_request_dir="./crawler_result/zhihu_bad_request_qid.tsv"):
    """
    获取知乎指定qid的response。
    Args:
     - qid_list: 
     - output_dataset_dir:
     - retry:
     - bad_request_dir:
    """
    
    if os.path.exists("./crawler_result/") == False:
        os.mkdir("./crawler_result/")
    
    for qid in qid_list:
        url = "https://www.zhihu.com/question/"+qid
        
        for i in range(retry):
            response = requests.post(url)
            time.sleep(0.5)
            if response.status_code == 200:
                break
        
        if response.status_code != 200:
            with open(bad_request_dir, "a", encoding="utf-8") as bad_request_file:
                bad_request_file.write(qid + "\n")
        else:
            with open(output_dataset_dir, "a", encoding="utf-8") as output_dataset_file:
                response_text = response.text.replace("\t", "")
                zhihu_answer_list = get_zhihu_answer_list(response_text)
                for zhihu_answer_dict in zhihu_answer_list:
                    output_list = [
                        zhihu_answer_dict["qid"], 
                        zhihu_answer_dict["content"],
                        zhihu_answer_dict["comment_count"],
                        zhihu_answer_dict["voteup_count"],
                        zhihu_answer_dict["thanks_count"],
                        zhihu_answer_dict["created_time"],
                        zhihu_answer_dict["updated_time"]
                    ]
                    output_list = [str(_) for _ in output_list]
                    output_dataset_file.write("\u0001".join(output_list) + "\n")
        
        time.sleep(0.5)

def get_zhihu_answer_list(zhihu_raw_text):
    """
    通过人工判断answers位置，编写正则提取answers字段的json。
    """
    zhihu_answer_list = []
    pattern = '(?<="answers":)\\{.*"zhiPlusExtraInfo":""\\}\\}'
    zhihu_response_json = re.search(pattern, zhihu_raw_text).group(0)
    zhihu_response_dict = json.loads(zhihu_response_json)
    for zhihu_response in zhihu_response_dict:
        zhihu_answer_dict = {}
        zhihu_answer_dict["qid"] = zhihu_response_dict[zhihu_response]["id"]
        zhihu_answer_dict["content"] = get_clean_text(zhihu_response_dict[zhihu_response]["content"])
        zhihu_answer_dict["comment_count"] = zhihu_response_dict[zhihu_response]["commentCount"]
        zhihu_answer_dict["voteup_count"] = zhihu_response_dict[zhihu_response]["voteupCount"]
        zhihu_answer_dict["thanks_count"] = zhihu_response_dict[zhihu_response]["thanksCount"]
        zhihu_answer_dict["created_time"] = zhihu_response_dict[zhihu_response]["createdTime"]
        zhihu_answer_dict["updated_time"] = zhihu_response_dict[zhihu_response]["updatedTime"]
        zhihu_answer_list.append(zhihu_answer_dict)

    return zhihu_answer_list

def get_clean_text(raw_text):
    """
    去掉html标签及其他符号。
    """
    clean_text = raw_text
    clean_pattern_list = [
        "<.*?>",
        "&.*?;"
    ]
    for clean_pattern in clean_pattern_list:
        clean_text = re.sub(clean_pattern, "", clean_text)
    return clean_text

if __name__ == "__main__":
    qid_list = ["34179857"]
    get_zhihu_results(qid_list)